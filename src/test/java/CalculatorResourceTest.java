import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+10";
        assertEquals(410, calculatorResource.calculate(expression));

        expression = " 300 - 99 - 10 ";
        assertEquals(191, calculatorResource.calculate(expression));

        expression = "4/2/2";
        assertEquals(1, calculatorResource.calculate(expression));

        expression = "2*4*5";
        assertEquals(40, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "300-100";
        assertEquals(200, calculatorResource.subtraction(expression));
    }
    
    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();
        
        String expression = "2*4";
        assertEquals(8, calculatorResource.multiplication(expression));

        expression = "4*2";
        assertEquals(8, calculatorResource.multiplication(expression));
    }
    
    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();
        
        String expression = "100/10";
        assertEquals(10, calculatorResource.division(expression));

        expression = "10/100";
        assertEquals(0, calculatorResource.division(expression));
    }
}
